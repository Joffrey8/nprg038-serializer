﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace UnreflectedSerializer
{
    public class PropertyOfDescriptor
    {
        internal string NameOfObject;
        internal Func<object, object> getter;
        internal Descriptor Desc;
    }
    /// <summary>
    /// Holds list of properties of particular class entity
    /// </summary>
    public class Descriptor
    {
        public List<PropertyOfDescriptor> ListOfProperties = new List<PropertyOfDescriptor>();

    }
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T">Type which is described</typeparam>
    public class Descriptor<T>:Descriptor
    {
        public void Add(string nameOfObject, Func<T, object> objectGetter)
        {
            ListOfProperties.Add(new PropertyOfDescriptor { NameOfObject = nameOfObject, getter = (object instance) => objectGetter.Invoke((T)instance), Desc = null });
        }
        public void Add(string nameOfObject, Func<T, object> objectGetter, Descriptor desc)
        {
            ListOfProperties.Add(new PropertyOfDescriptor { NameOfObject = nameOfObject, getter = (object instance) => objectGetter.Invoke((T)instance), Desc = desc });
        }
        }
    
    public class RootDescriptor<T>:Descriptor<T>
    {
        public void Serialize(TextWriter writer, T property)
        {
            recursiveSerialize(writer, property, typeof(T).Name, this);
        }
        /// <summary>
        /// recursively calls every property of class (if Descriptor is not null) and prints XML output.
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="property"></param>
        /// <param name="name"></param>
        /// <param name="d"></param>
        private void recursiveSerialize(TextWriter writer, object property, string name, Descriptor d)
        {
            if (d == null)
            {
                writer.WriteLine("<" + name + ">" + property.ToString() + "</" + name + ">");
            }
            else
            {
                writer.WriteLine("<" + name + ">");
                foreach (PropertyOfDescriptor m in d.ListOfProperties)
                    recursiveSerialize(writer, m.getter(property), m.NameOfObject, m.Desc);
                writer.WriteLine("</" + name + ">");
            }

        }
    }

    class Address
    {
        public string Street { get; set; }
        public string City { get; set; }
    }

    class Country
    {
        public string Name { get; set; }
        public int AreaCode { get; set; }
    }

    class PhoneNumber
    {
        public Country Country { get; set; }
        public int Number { get; set; }
    }

    class Person
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Address HomeAddress { get; set; }
        public Address WorkAddress { get; set; }
        public Country CitizenOf { get; set; }
        public PhoneNumber MobilePhone { get; set; }
    }

    class Program
    {
        static void Main(string[] args)
        {
            RootDescriptor<Person> rootDesc = GetPersonDescriptor();

            var czechRepublic = new Country { Name = "Czech Republic", AreaCode = 420 };
            var person = new Person
            {
                FirstName = "Pavel",
                LastName = "Jezek",
                HomeAddress = new Address { Street = "Patkova", City = "Prague" },
                WorkAddress = new Address { Street = "Malostranske namesti", City = "Prague" },
                CitizenOf = czechRepublic,
                MobilePhone = new PhoneNumber { Country = czechRepublic, Number = 123456789 }
            };

            rootDesc.Serialize(Console.Out, person);
            Console.Read();
        }

        static RootDescriptor<Person> GetPersonDescriptor()
        {
            var rootDesc = new RootDescriptor<Person>();
            var address = new RootDescriptor<Address>();
            var country = new RootDescriptor<Country>();
            var phoneNum = new RootDescriptor<PhoneNumber>();
            address.Add("Street", x => x.Street);
            address.Add("City", x => x.City);
            country.Add("Name", x => x.Name);
            country.Add("AreaCode", x => x.AreaCode.ToString());
            phoneNum.Add("Country", x => x.Country, country);
            phoneNum.Add("Number", x => x.Number.ToString());
            rootDesc.Add("FirstName", x => x.FirstName);
            rootDesc.Add("LastName", x => x.LastName);
            rootDesc.Add("HomeAddress", x => x.HomeAddress, address);
            rootDesc.Add("WorkAddress", x => x.WorkAddress, address);

            rootDesc.Add("CitizenOf", x => x.CitizenOf, country);
            rootDesc.Add("MobilePhone", x => x.MobilePhone, phoneNum);



            return rootDesc;
        }
    }
}